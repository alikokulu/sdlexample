#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#define WIDTH 800
#define HEIGHT 600
#define IMG_PATH "ship.png"

// Main function.
int main(int argc, char* args[]) // Main MUST have these parameters for SDL.
{
	//variable declarations
	SDL_Window *win = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Texture *img = NULL;
	int w, h;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return 1;

	//create the window and the renderer
	win = SDL_CreateWindow("SDL Example", 100, 100, WIDTH, HEIGHT, 0);
	renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

	//load the image
	img = IMG_LoadTexture(renderer, IMG_PATH);

	//get the width and height of the texture
	SDL_QueryTexture(img, NULL, NULL, &w, &h);

	//put the location where we want the image to be drawn into a rectangle
	SDL_Rect texr;
	texr.x = (WIDTH / 2) - w;
	texr.y = (HEIGHT / 2) - h;
	texr.w = w * 2;
	texr.h = h * 2;

	//main loop
	while (true)
	{
		//event handling
		SDL_Event e;
		if (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
				break;
			else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE)
				break;
		}

		//clear the screen
		SDL_RenderClear(renderer);
		//copies texture to rendering context
		SDL_RenderCopy(renderer, img, NULL, &texr);
		//flip the backbuffer (everything we prepared behind the screen is shown)
		SDL_RenderPresent(renderer);
	}

	SDL_DestroyTexture(img);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(win);

	return 0;

}